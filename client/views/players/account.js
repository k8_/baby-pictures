Template.account.helpers({
  name: function() {
    var value,
        name = Meteor.user().profile.name;

    if (name) {
      value = name;
    } else {
      var mail = Meteor.users.findOne(Meteor.userId()).emails[0].address;
          result = /(\w+).(\w+)@/.exec(mail);

      value = result[1].charAt(0).toUpperCase() + result[1].substr(1).toLowerCase() + ' ' + result[2].charAt(0).toUpperCase() + result[2].substr(1).toLowerCase();
    }
    return value;
  }
});

Template.account.events({
  'submit form': function(e) {
    var name = $('[name="name"]').val();

    e.preventDefault();
    console.log(e);

    // Set name, if needed.
    Meteor.users.update(
      { _id: Meteor.userId() },
      { $set: { "profile.name": name } }
    );

    // Upload image
    FS.Utility.eachFile(e, function(file) {
      Images.insert(file, function (err, fileObj) {
        //Inserted new doc with ID fileObj._id, and kicked off the data upload using HTTP
      });
    });
  }
})
