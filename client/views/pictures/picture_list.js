Template.pictureList.helpers({
  pictures: function() {
    return Pictures.find({}, { sort: { _id: -1 } });
  }
});

var droppedInZone = false;

Template.pictureList.events({
  // "submit" the stuff
  'submit form': function(e) {
    var choices = new Array();

    e.preventDefault();

    // user hasn't played yet
    if (!Meteor.user().profile.played) {
      $('.pictures__picture').each(function() {
        var pictureId = $(this).find('.pictures__image').attr('data-id'),
            match = $(this).find('.pictures__match').text(),
            pictureInfo = Players.findOne({id: pictureId});

        choices.push({
          'id': pictureInfo.name,
          'match': match,
          'correct': pictureInfo.name === match
        });
      });

      Meteor.call('post', choices, function(error, id) {
        if (error) {
          Errors.throw(error.reason);

          if (error.error === 302) {
            Router.go('results', {_id: error.details});
          }
        } else {
          Router.go('results');
        }

        Router.go('results');
      });
    } else {
      Errors.throw('Cheater! You\'ve already played!');
      Router.go('results');
    }
  },
  // Draggable Start
  'dragstart .players__player': function(e) {
    var transfer = e.originalEvent.dataTransfer;

    transfer.dropEffect = 'move';
    transfer.effectAllowed = 'move';
    transfer.setData('btn', $(e.target).html());
  },

  'dragend .players__player': function(e) {
    if (!!droppedInZone) {
      $(e.target).addClass('btn-danger').removeAttr('draggable');
      droppedInZone = false;
    }
  },

  // Set draggable areas
  'dragenter .pictures__match.matchable': function(e) {
    cancel(e);

    $(e.target).addClass('btn-success');
  },
  'dragover .pictures__match': cancel,
  'dragleave .pictures__match.matchable': function(e) {
    cancel(e);

    $(e.target).removeClass('btn-success');
  },

  // Drop the right object
  'drop .pictures__match.matchable': function(e) {
    droppedInZone = true;
    $(e.target).append(e.originalEvent.dataTransfer.getData('btn'));
    $(e.target).removeClass('btn-success').removeClass('matchable');

    return false;
  }
});

function cancel(e) {
  if (e.preventDefault) {
    e.preventDefault();
  }
  return false;
}
