Template.picture.helpers({
  played: function() {
    return !!Meteor.user().profile.played;
  }
})