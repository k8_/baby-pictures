Template.results.helpers({
  results: function() {
    return Results.find({}, {sort: {correct: -1}});
  },
  total: function() {
    var players = Players.find();

    return players.count();
  }
});
