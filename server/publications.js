Meteor.publish('results', function() {
  return Results.find();
});

Meteor.publish('pictures', function() {
  return Pictures.find();
});

Meteor.publish('players', function() {
  return Players.find({}, {sort: { name: 1 } });
});