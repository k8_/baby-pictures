if (Players.find().count() === 0) {
  var players = [
    'Aastha Verma',
    'Betsy Barefoot',
    'Bob McGrath',
    'Brenda Stewart',
    'Don Sargent',
    'Don Liggett',
    'Greg Channel',
    'Jacques Benovil',
    'Jojo Mendoza',
    'Joe Clark',
    'John Ready',
    'Julius Foster',
    'Kimler Corey',
    'Karla Haworth',
    'Kate Green',
    'Kenny Moir',
    'Kirsten Wirth',
    'Marcy Walker',
    'Matt Bassow',
    'Mike Fernandez',
    'Murali Thota',
    'Ralph Gioseffi',
    'Robert Boggs',
    'Steve Smith',
    'Todd Messer',
    'Rob Watson',
    'Cindy Kennedy'
    ];

  players.forEach(function(val,key) {
    var names = val.match(/(\w+)\s(\w+)/);

    var userId = Meteor.users.insert({
      username: names[1].toLowerCase() + names[2].toLowerCase(),
      profile: { name: val }
    });

    Accounts.setPassword(userId, names[2].toLowerCase());

    Players.insert({ id: userId, name: val });

    Pictures.insert({
      player: userId,
      name: val,
      src: key + '.png'
    });
  });
}
