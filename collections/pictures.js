Pictures = new Meteor.Collection('pictures');

Pictures.allow({
  update: ownsDocument
});
