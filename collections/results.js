Results = new Meteor.Collection('results');

Meteor.methods({
  post: function(choices) {
    var user = Meteor.user(),
        total = 0;

    // ensure the user is logged in
    if (!user) {
      throw new Meteor.Error(401, "You need to log in to play.");
    }

    choices.forEach(function(val) {
      total = !!val.correct ? total + 1 : total;
    });

    var result = {
      player: user.profile.name,
      play: choices,
      correct: total
    };

    var resultId = Results.insert(result);

    Meteor.users.update(
      { _id: user._id },
      { $set: { 'profile.played': resultId } }
    );
  }
});
