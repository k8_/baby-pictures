Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading'
});

Router.map(function() {
  this.route('account', {
    path: '/account',
    data: function() { return Meteor.users.findOne(Meteor.userId()) }
  });

  this.route('pictureList', {
    path: '/play',
    waitOn: function() {
      return [
        Meteor.subscribe('pictures'),
        Meteor.subscribe('players')
      ];
    }
  });

  // results
  this.route('results', {
    path: '/results',
    waitOn: function() {
      return [
        Meteor.subscribe('results'),
        Meteor.subscribe('players')
      ]
    }
  });

  var requireLogin = function requireLogin(pause) {
  if (!Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    }
    this.render('accessDenied');
    pause();
  }
};

Router.onBeforeAction('loading');
Router.onBeforeAction(requireLogin, {only: 'pictureList'} );
});
